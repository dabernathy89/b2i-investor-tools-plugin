# b2i Investor Tools #
**Contributors:**      b2ijoe
**Tags:**
**Requires at least:** 4.4
**Tested up to:**      4.6
**Stable tag:**        0.5.3
**License:**           GPLv2
**License URI:**       http://www.gnu.org/licenses/gpl-2.0.html

## Description ##

We offer all the needed plugins, data and much more, for handling day to day Investor relations tasks on your website.

Our plugins include: SEC filings, stock data, stock chart, press releases, showcase items like your latest presentation, road show, or calendar of events items.

Plugins have numerous customization parameters for complete control of data display.

## Installation ##

### Manual Installation ###

1. Upload the entire `/b2i` directory to the `/wp-content/plugins/` directory.
2. Activate b2i through the 'Plugins' menu in WordPress.

## Changelog ##

### 0.5.3 ###
* Initial public release
