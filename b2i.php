<?php
/**
 * Plugin Name: b2i Investor Tools
 * Plugin URI:  http://www.b2itech.com
 * Description: SEC, Stock chart and Quote data, Press releases and more... One source for all your stock data plugins.
 * Version:     0.5.3
 * Author:      b2itech
 * Author URI:  http://www.b2itech.com
 * License:     GPLv2
 * Text Domain: b2i
 * Domain Path: /languages
 *
 * @link http://www.b2itech.com
 *
 * @package b2i
 * @version 0.5.3
 */

/**
 * Copyright (c) 2016 b2itech (email : info@b2itech.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License, version 2 or, at
 * your discretion, any later version, as published by the Free
 * Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

/**
 * Built using generator-plugin-wp
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Autoloads files with classes when needed
 *
 * @since  0.2.0
 * @param  string $class_name Name of the class being requested.
 * @return void
 */
function b2i_autoload_classes( $class_name ) {
	if ( 0 !== strpos( $class_name, 'B2i_' ) ) {
		return;
	}

	$filename = strtolower( str_replace(
		'_', '-',
		substr( $class_name, strlen( 'B2i_' ) )
	) );

	B2i::include_file( $filename );
}
spl_autoload_register( 'b2i_autoload_classes' );

/**
 * Main initiation class
 *
 * @since  0.2.0
 */
final class B2i {

	/**
	 * Current version
	 *
	 * @var  string
	 * @since  0.2.0
	 */
	const VERSION = '0.5.3';

	/**
	 * URL of plugin directory
	 *
	 * @var string
	 * @since  0.2.0
	 */
	protected $url = '';

	/**
	 * Path of plugin directory
	 *
	 * @var string
	 * @since  0.2.0
	 */
	protected $path = '';

	/**
	 * Plugin basename
	 *
	 * @var string
	 * @since  0.2.0
	 */
	protected $basename = '';

	/**
	 * Singleton instance of plugin
	 *
	 * @var B2i
	 * @since  0.2.0
	 */
	protected static $single_instance = null;

	/**
	 * Instance of B2i_Shortcode
	 *
	 * @since 0.2.0
	 * @var B2i_Shortcode
	 */
	protected $shortcode;

	/**
	 * Instance of B2i_Options
	 *
	 * @since 0.2.0
	 * @var B2i_Options
	 */
	protected $options;

	/**
	 * Creates or returns an instance of this class.
	 *
	 * @since  0.2.0
	 * @return B2i A single instance of this class.
	 */
	public static function get_instance() {
		if ( null === self::$single_instance ) {
			self::$single_instance = new self();
		}

		return self::$single_instance;
	}

	/**
	 * Sets up our plugin
	 *
	 * @since  0.2.0
	 */
	protected function __construct() {
		$this->basename = plugin_basename( __FILE__ );
		$this->url      = plugin_dir_url( __FILE__ );
		$this->path     = plugin_dir_path( __FILE__ );
	}

	/**
	 * Attach other plugin classes to the base plugin class.
	 *
	 * @since  0.2.0
	 * @return void
	 */
	public function plugin_classes() {
		// Attach other plugin classes to the base plugin class.
		$this->options = new B2i_Options( $this );
		$this->shortcode = new B2i_Shortcode( $this );
	} // END OF PLUGIN CLASSES FUNCTION

	/**
	 * Add hooks and filters
	 *
	 * @since  0.2.0
	 * @return void
	 */
	public function hooks() {
		add_action( 'init', array( $this, 'init' ) );
		add_action( 'admin_notices', array( $this, 'maybe_display_activation_notice' ) );
	}

	/**
	 * Set the activation message if the plugin was just activated
	 *
	 * @since  0.2.0
	 * @return void
	 */
	public function set_activation_message() {
		$html = '<div class="notice notice-info is-dismissable">';
		$html .= '<p>';
		$html .= __( 'Thank you for installing the b2i plugin! Visit the <a href="' . admin_url( 'options-general.php?page=b2i_options' ) . '">settings page</a> to get started.', 'b2i' );
		$html .= '</p>';
		$html .= '</div>';

		update_option( 'b2i_activation_message', $html );
	}

	/**
	 * Display the activation message if it is set
	 *
	 * @since  0.2.0
	 * @return void
	 */
	public function maybe_display_activation_notice() {
		$message = get_option( 'b2i_activation_message', false );

		if ( $message ) {
			update_option( 'b2i_activation_message', false );
			echo $message;
		}
	}

	/**
	 * Activate the plugin
	 *
	 * @since  0.2.0
	 * @return void
	 */
	public function _activate() {
		// Make sure any rewrite functionality has been loaded.
		flush_rewrite_rules();
		$this->set_activation_message();
	}

	/**
	 * Deactivate the plugin
	 * Uninstall routines should be in uninstall.php
	 *
	 * @since  0.2.0
	 * @return void
	 */
	public function _deactivate() {}

	/**
	 * Init hooks
	 *
	 * @since  0.2.0
	 * @return void
	 */
	public function init() {
		load_plugin_textdomain( 'b2i', false, dirname( $this->basename ) . '/languages/' );
		$this->plugin_classes();
	}

	/**
	 * Deactivates this plugin, hook this function on admin_init.
	 *
	 * @since  0.2.0
	 * @return void
	 */
	public function deactivate_me() {
		deactivate_plugins( $this->basename );
	}

	/**
	 * Magic getter for our object.
	 *
	 * @since  0.2.0
	 * @param string $field Field to get.
	 * @throws Exception Throws an exception if the field is invalid.
	 * @return mixed
	 */
	public function __get( $field ) {
		switch ( $field ) {
			case 'version':
				return self::VERSION;
			case 'basename':
			case 'url':
			case 'path':
			case 'shortcode':
			case 'options':
				return $this->$field;
			default:
				throw new Exception( 'Invalid '. __CLASS__ .' property: ' . $field );
		}
	}

	/**
	 * Include a file from the includes directory
	 *
	 * @since  0.2.0
	 * @param  string $filename Name of the file to be included.
	 * @return bool   Result of include call.
	 */
	public static function include_file( $filename ) {
		$file = self::dir( 'includes/class-'. $filename .'.php' );
		if ( file_exists( $file ) ) {
			return include_once( $file );
		}
		return false;
	}

	/**
	 * This plugin's directory
	 *
	 * @since  0.2.0
	 * @param  string $path (optional) appended path.
	 * @return string       Directory and path
	 */
	public static function dir( $path = '' ) {
		static $dir;
		$dir = $dir ? $dir : trailingslashit( dirname( __FILE__ ) );
		return $dir . $path;
	}

	/**
	 * This plugin's url
	 *
	 * @since  0.2.0
	 * @param  string $path (optional) appended path.
	 * @return string       URL and path
	 */
	public static function url( $path = '' ) {
		static $url;
		$url = $url ? $url : trailingslashit( plugin_dir_url( __FILE__ ) );
		return $url . $path;
	}
}

/**
 * Grab the B2i object and return it.
 * Wrapper for B2i::get_instance()
 *
 * @since  0.2.0
 * @return B2i  Singleton instance of plugin class.
 */
function b2i() {
	return B2i::get_instance();
}

// Kick it off.
add_action( 'plugins_loaded', array( b2i(), 'hooks' ) );

register_activation_hook( __FILE__, array( b2i(), '_activate' ) );
register_deactivation_hook( __FILE__, array( b2i(), '_deactivate' ) );
