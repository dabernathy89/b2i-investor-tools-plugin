<?php
/**
 * B2i Shortcode
 *
 * @since 0.1.0
 * @package b2i
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * B2i Shortcode.
 *
 * @since 0.1.0
 */
class B2i_Shortcode {
	/**
	 * Parent plugin class
	 *
	 * @var   class
	 * @since 0.1.0
	 */
	protected $plugin = null;

	/**
	 * Business ID option
	 *
	 * @var   string
	 * @since 0.1.0
	 */
	protected $business_id = '';

	/**
	 * Key option
	 *
	 * @var   string
	 * @since 0.1.0
	 */
	protected $key = '';

	/**
	 * Base URL
	 *
	 * @var   string
	 * @since 0.1.0
	 */
	protected $base_url = 'http://www.b2i.us/b2i/';

	/**
	 * Constructor
	 *
	 * @since  0.1.0
	 * @param  object $plugin Main plugin object.
	 * @return void
	 */
	public function __construct( $plugin ) {
		$this->plugin = $plugin;

		add_action( 'cmb2_init', array( $this, 'setup_vars' ) );
		add_action( 'cmb2_init', array( $this, 'shortcodes' ) );
	}

	/**
	 * Setup business id and api key variables
	 *
	 * @since  0.1.0
	 * @return void
	 */
	public function setup_vars() {
		$this->business_id = cmb2_get_option( 'b2i_options', 'business_id' );
		$this->key = cmb2_get_option( 'b2i_options', 'key' );
	}

	/**
	 * Initiate shortcodes
	 *
	 * @since  0.1.0
	 * @return void
	 */
	public function shortcodes() {
		add_shortcode( 'b2i_library_latest_item', array( $this, 'library_latest_item_shortcode' ) );
		add_shortcode( 'b2i_library_headline', array( $this, 'library_headline_shortcode' ) );
		add_shortcode( 'b2i_press_releases', array( $this, 'press_releases_shortcode' ) );
		add_shortcode( 'b2i_sec', array( $this, 'sec_shortcode' ) );
		add_shortcode( 'b2i_quote', array( $this, 'quote_shortcode' ) );
		add_shortcode( 'b2i_showcase', array( $this, 'showcase_shortcode' ) );
		add_shortcode( 'b2i_email_optin', array( $this, 'email_optin_shortcode' ) );
	}

	/**
	 * Define the Library single plugin shortcode
	 *
	 * @since  0.1.0
	 * @return string
	 */
	public function library_latest_item_shortcode( $atts ) {
		$atts = shortcode_atts(
			array(
				'g' => '',
				't' => '',
				'f' => '',
				'i' => '5',
				'off' => '',
				'out' => '',
				'ln' => '',
				'n' => '',
			),
			$atts
		);

		$url_args = array(
			'b' => $this->business_id,
			'n' => '1',
			's' => '0',
			'l' => '1',
			'i' => $atts['i'],
			'g' => $atts['g'],
			't' => $atts['t'],
			'f' => $atts['f'],
			'off' => $atts['off'],
			'out' => $atts['out'],
			'ln' => $atts['ln'],
			'n' => $atts['n']
		);

		$url_args = array_filter( $url_args, array( $this, 'remove_empty_url_args' ) );

		$url = add_query_arg( $url_args, $this->base_url . 'SingleHeadlinePluginData.asp' );

		$html = '<script language="JavaScript" src="' . esc_url( $url ) . '" type="text/javascript"></script>';

		return $html;
	}

	/**
	 * Define the Library plugin shortcode
	 *
	 * @since  0.1.0
	 * @return string
	 */
	public function library_headline_shortcode( $atts ) {
		$atts = shortcode_atts(
			array(
				'g' => '',
				't' => '',
				'f' => '',
				'i' => '5'
			),
			$atts
		);

		$url_args = array(
			'b' => $this->business_id,
			'n' => '1',
			's' => '0',
			'l' => '1',
			'i' => $atts['i'],
			'g' => $atts['g'],
			't' => $atts['t'],
			'f' => $atts['f'],
		);

		$url_args = array_filter( $url_args, array( $this, 'remove_empty_url_args' ) );

		$url = add_query_arg( $url_args, $this->base_url . 'HeadlinePlugin.asp' );

		$html = '<script language="JavaScript" src="' . esc_url( $url ) . '" type="text/javascript"></script>';

		return $html;
	}

	/**
	 * Define the Library Standalone plugin shortcode
	 *
	 * @since  0.1.0
	 * @return string
	 */
	public function press_releases_shortcode( $atts ) {
		$html = '';

		$atts = shortcode_atts(
			array(
				'group' => '',
				'snav' => '',
			),
			$atts
		);

		$html .= '<div id="LibDiv"></div>' . "\n";
		$html .= '<script language="JavaScript" src="' . esc_url( $this->base_url . 'LibraryApi.js' ) . '" type="text/javascript"></script>' . "\n";
		$html .= '<script language="JavaScript" type="text/javascript">' . "\n";
		$html .= 'oLib.BizID="' . $this->business_id . '";' . "\n";
		$html .= 'oLib.Group="' . esc_attr( $atts['group'] ) . '";' . "\n";
		$html .= ( $atts['snav'] !== '' ) ? 'oLib.sNav="' . esc_attr( $atts['snav'] ) . '";' . "\n" : '';
		$html .= 'getData();' . "\n";
		$html .= '</script>' . "\n";

		return $html;
	}

	/**
	 * Define the SEC Standalone plugin shortcode
	 *
	 * @since  0.1.0
	 * @return string
	 */
	public function sec_shortcode( $atts ) {
		$html = '';

		$atts = shortcode_atts(
			array(
				'y' => '',
				't' => '',
				'o' => 'default',
				'c' => '',
				'n' => '',
				'sf' => '',
				'sh' => '',
				'lo' => '1',
			),
			$atts
		);

		$html .= '<div id="SECdiv"></div>' . "\n";
		$html .= '<script language="JavaScript" src="' . esc_url( $this->base_url . 'SECapi.js' ) . '" type="text/javascript"></script>' . "\n";
		$html .= '<script language="JavaScript" type="text/javascript">' . "\n";
		$html .= 'oSEC.BizID="' . $this->business_id . '";' . "\n";
		$html .= 'oSEC.sKey="' . $this->key . '";' . "\n";

		$html .= ( $atts['y'] !== '' ) ? 'oSEC.y="' . esc_attr( $atts['y'] ) . '";' . "\n" : '';
		$html .= ( $atts['t'] !== '' ) ? 'oSEC.t="' . esc_attr( $atts['t'] ) . '";' . "\n" : '';
		$html .= ( $atts['o'] !== '' ) ? 'oSEC.o="' . esc_attr( $atts['o'] ) . '";' . "\n" : '';
		$html .= ( $atts['c'] !== '' ) ? 'oSEC.c="' . esc_attr( $atts['c'] ) . '";' . "\n" : '';
		$html .= ( $atts['n'] !== '' ) ? 'oSEC.n="' . esc_attr( $atts['n'] ) . '";' . "\n" : '';
		$html .= ( $atts['sf'] !== '' ) ? 'oSEC.sf="' . esc_attr( $atts['sf'] ) . '";' . "\n" : '';
		$html .= ( $atts['sh'] !== '' ) ? 'oSEC.sh="' . esc_attr( $atts['sh'] ) . '";' . "\n" : '';
		$html .= ( $atts['lo'] !== '' ) ? 'oSEC.lo="' . esc_attr( $atts['lo'] ) . '";' . "\n" : '';

		$html .= 'getSecData();' . "\n";
		$html .= '</script>' . "\n";

		return $html;
	}

	/**
	 * Define the Quote plugin shortcode
	 *
	 * @since  0.1.0
	 * @return string
	 */
	public function quote_shortcode( $atts ) {
		$html = '';

		$atts = shortcode_atts(
			array(
				'dataonly' => 'false',
				's' => ''
			),
			$atts
		);

		$endpoint = $atts['dataonly'] === 'true' ? 'quote3.asp' : 'QuotePlug.asp';

		$url_args = array(
			'b' => $this->business_id,
			'L' => '1',
			's' => $atts['s']
		);

		$url = add_query_arg( $url_args, $this->base_url . $endpoint );

		$html .= '<script language="JavaScript" src="' . esc_url( $url ) . '" type="text/javascript"></script>' . "\n";

		return $html;
	}

	/**
	 * Define the Custom Showcase plugin shortcode
	 *
	 * @since  0.1.0
	 * @return string
	 */
	public function showcase_shortcode( $atts ) {
		$html = '';

		$atts = shortcode_atts(
			array(
				'display' => 'calendar',
				'c' => '5',
				'out' => '',
				'i' => '',
				'd' => '',
				'np' => '',
				'nw' => '',
				'df' => '',
				'ds' => ''
			),
			$atts
		);

		$showcases = array(
			'calendar' => '1',
			'files' => '19',
			'financials' => '2',
			'presentations' => '3'
		);

		$url_args = array(
			'b' => $this->business_id,
			'P' => 'I',
			'L' => '1',
			'N' => '1',
			'S' => '0',
			'ID' => $showcases[$atts['display']],
			'c' => $atts['c'],
			'out' => $atts['out'],
			'i' => $atts['i'],
			'd' => $atts['d'],
			'np' => $atts['np'],
			'nw' => $atts['nw'],
			'df' => $atts['df'],
			'ds' => $atts['ds']
		);

		$url = add_query_arg( $url_args, $this->base_url . 'showcaselist.asp' );

		$url_args = array_filter( $url_args, array( $this, 'remove_empty_url_args' ) );

		$html .= '<script language="JavaScript" src="' . esc_url( $url ) . '" type="text/javascript"></script>' . "\n";

		return $html;
	}

	/**
	 * Define the Email Opt-in plugin shortcode
	 *
	 * @since  0.1.0
	 * @return string
	 */
	public function email_optin_shortcode( $atts ) {
		$html = '';

		$url_args = array(
			'b' => $this->business_id,
			'L' => '1',
			'S' => '0'
		);

		$url = add_query_arg( $url_args, $this->base_url . 'emailplug.asp' );

		$html .= '<script language="JavaScript" src="' . esc_url( $url ) . '" type="text/javascript"></script>' . "\n";

		return $html;
	}

	protected function remove_empty_url_args( $arg ) {
		return ( $arg !== '' );
	}
}
