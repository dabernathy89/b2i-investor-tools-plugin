<div class="wrap">
    <br><hr><br>
    <p><h3>Shortcodes</h3></p>
    <p>Examples:</p>
    <p><code>[b2i_library_latest_item g="65"]</code></p>
    <p><code>[b2i_quote s="EAT"]</code></p>
    <p><code>[b2i_sec lo="2" c="5" t="10" sf="0"]</code></p>
    <table class="b2i-instructions widefat fixed">
        <thead>
            <tr>
                <th>b2i Plugin</th>
                <th>Shortcode</th>
                <th>Attributes</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Press releases plugin</td>
                <td>[b2i_press_releases]</td>
                <td>
                    <strong>group</strong> - Library ID<br>
                    <strong>snav</strong> - Set to 0 to hide the navigation
                </td>
            </tr>
            <tr>
                <td>SEC plugin</td>
                <td>[b2i_sec]</td>
                <td>
                    <strong>y</strong> - Year, ex. y=2016 to show only filings for this year<br>
                    <strong>t</strong> - Type, ex. t=10k to show only 10k filings<br>
                    <strong>o</strong> - Owner: 'default' = all filings, '1' = owner only filings, '2' = exclude owner filings<br>
                    <strong>c</strong> - Count of filings per page<br>
                    <strong>n</strong> - Set to 0 to hide nav<br>
                    <strong>sf</strong> - Set to 0 to hide search/filter form<br>
                    <strong>sh</strong> - Set to 0 to hide header row with column titles<br>
                    <strong>lo</strong> - Layout: default '1' (Table), set to '2' for Div output
                </td>
            </tr>
            <tr>
                <td>Quote plugin</td>
                <td>[b2i_quote]</td>
                <td>
                    <strong>dataonly</strong> - "true" to return data only (default "false")<br>
                    <strong>s</strong> - stock symbol to look up<br>
                </td>
            </tr>
            <tr>
                <td>Library latest item plugin</td>
                <td>[b2i_library_latest_item]</td>
                <td>
                    <strong>g</strong> - Library ID<br>
                    <strong>t</strong> - Tag<br>
                    <strong>f</strong> - Filter<br>
                    <strong>i</strong> - Number of items to return<br>
                    <strong>off</strong> - Number of items to offset the start for displaying<br>
                    <strong>out</strong> - Set to 1 to output an HTML table<br>
                    <strong>ln</strong> - Stands for length or count of characters to return for each item. If you do not include this, the default is 200.<br>
                    <strong>n</strong> - Set to 1 to allow the basic navigation to show when visitors click the item links. Including the navigation parameter overrides the setting in the Headline display section.<br>
                </td>
            </tr>
            <tr>
                <td>Library headline display plugin</td>
                <td>[b2i_library_headline]</td>
                <td>
                    <strong>i</strong> - Number of items to return<br>
                    <strong>g</strong> - Library ID<br>
                    <strong>t</strong> - Tag<br>
                    <strong>f</strong> - Filter
                </td>
            </tr>
            <tr>
                <td>Showcase plugin</td>
                <td>[b2i_showcase]</td>
                <td>
                    <strong>display</strong> - Options: 'calendar', 'files', 'financials', 'presentations' (default 'calendar')<br>
                    <strong>c</strong> - Control the number of items displayed - default is 5 when omitted<br>
                    <strong>out</strong> - Set to 1 to output an unordered list instead of a table<br>
                    <strong>I</strong> - Set to 1 to show the icon for the item<br>
                    <strong>D</strong> - Set to 1 to show the date for the item<br>
                    <strong>np</strong> - Set to 1 to override the downloads popout setting<br>
                    <strong>nw</strong> - Set to 1 to make the downloads popout when your site does not<br>
                    <strong>df</strong> - Set to 2 to alter the date formatting<br>
                    <strong>ds</strong> - Set to 1 to sort by date ascending instead of descending; Set to 2 to sort by the title
                </td>
            </tr>
            <tr>
                <td>Email Opt-In plugin</td>
                <td>[b2i_email_optin]</td>
                <td></td>
            </tr>
        </tbody>
    </table>
</div>