<div class="wrap">
    <h2>Thank you for installing the b2i plugin!</h2>
    <p>If you already have an account with b2i, you can visit your account settings to find your Business ID and Application ID.</p>
    <p><strong>If you do not have an account with b2i, visit <a href="<?php echo esc_attr( $url ); ?>">this page</a> to get started!
    </strong></p>
</div>